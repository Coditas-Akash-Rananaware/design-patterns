function Developer(name){
    this.name=name;
    this.type="Developer";
}

function Tester(name){
    this.name=name;
    this.type="Tester";
}


function EmployeeFactory(){
    this.create=(name,type)=>{
        switch(type){
            case 1:
                return new Developer(name);
                break;
            case 2:
                return new Tester(name);
                break;
        }
    };
}
function say(){
    console.log(`I am a ${this.type} and my name is ${this.name}`);
}
const employeeFactory=new EmployeeFactory();
const employees=[];
employees.push(employeeFactory.create("ABC",1));
employees.push(employeeFactory.create("PQR",2));
employees.push(employeeFactory.create("XYZ", 1));

employees.forEach((emp)=>{
    say.call(emp);
})